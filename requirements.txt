pynautobot==1.0.4
netaddr==0.8.0
netifaces==0.10.9
pyyaml==5.4.1
jsonargparse==3.11.2
