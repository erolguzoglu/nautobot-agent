from setuptools import find_packages, setup

setup(
    name='nautobot_agent',
    version='0.0.1',
    description='Nautobot agent for server',
    long_description=open('README.md', encoding="utf-8").read(),
    long_description_content_type='text/markdown',
    url='https://gitlab.trendyol.com/platform/infra/tools/nautobot-agent',
    author='Erol Guzoğlu',
    author_email='erol.guzoglu@trendyol.com',
    license='Apache2',
    include_package_data=True,
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"]),
    use_scm_version=True,
    install_requires=[
        'pynautobot==1.0.4',
        'netaddr==0.8.0',
        'netifaces==0.10.9',
        'pyyaml==5.4.1',
        'jsonargparse==2.32.2',
    ],
    zip_safe=False,
    keywords=['nautobot'],
    classifiers=[
        'Intended Audience :: Developers',
        'Development Status :: 5 - Production/Stable',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    entry_points={
        'console_scripts': ['nautobot_agent=nautobot_agent.cli:main'],
    }
)
